// const hadDetectionKeyPoints = {
//   0: wrist,
//   1: thumb_cmc,
//   2: thumb_mcp,
//   3: thumb_ip,
//   4: thumb_tip,
//   5: index_finger_mcp,
//   6: index_finger_pip,
//   7: index_finger_dip,
//   8: index_finger_tip,
//   9: middle_finger_mcp,
//   10: middle_finger_pip,
//   11: middle_finger_dip,
//   12: middle_finger_tip,
//   13: ring_finger_mcp,
//   14: ring_finger_pip,
//   15: ring_finger_dip,
//   16: ring_finger_tip,
//   17: pinky_finger_mcp,
//   18: pinky_finger_pip,
//   19: pinky_finger_dip,
//   20: pinky_finger_tip,
// };

function __createOrUpdateVideo(context) {
  const mainDiv = context.shadowRoot.getElementById(context.mainHandDiv);

  const videoId = context.videoTagId;
  const canvasId = context.canvasTagId;

  const videoDiv = mainDiv.querySelector(`#${videoId}`);
  const canvasDiv = mainDiv.querySelector(`#${canvasId}`);
  // Video Element is not present
  // Creating a videoDiv
  if (!videoDiv) {
    const videoTag = document.createElement("video");
    videoTag.setAttribute("id", videoId);
    videoTag.setAttribute("autoplay", true);
    videoTag.setAttribute("muted", true);
    videoTag.setAttribute("playsinline", true);
    videoTag.width = context.default_width;
    videoTag.height = context.default_height;
    //Styling
    videoTag.style.visibility = "hidden";
    videoTag.style.position = "absolute";
    videoTag.style.top = "0px";
    videoTag.style.left = "0px";
    // videoTag.style.transform = "scaleX(-1)"
    mainDiv.appendChild(videoTag);

    //Creating Loading GIF
    const loadingDiv = document.createElement("div");
    loadingDiv.setAttribute("id", context.loadingTagId)
    loadingDiv.style.position = "absolute";
    loadingDiv.style.width = `${Number(context.default_width) + 1}px`
    loadingDiv.style.height = `${context.default_height}px`
    loadingDiv.style.background = `white`
    loadingDiv.style.display = `flex`
    loadingDiv.style.justifyContent = `center`
    loadingDiv.style.alignItems = `center`
    loadingDiv.style.boxSizing = `border-box`
    loadingDiv.style.borderLeft = `2px solid black`
    loadingDiv.style.borderRight = `2px solid black`
        // Crete Loading Text
      const loadingGif =document.createElement('img')
      loadingGif.src = "./assets/loading.gif"
      loadingDiv.appendChild(loadingGif)
    mainDiv.appendChild(loadingDiv);

    if (!canvasDiv) {
      const canvasTag = document.createElement("canvas");
      canvasTag.setAttribute("id", canvasId);
      canvasTag.style.transform = "rotateY(180deg)";
      canvasTag.width = context.default_width;
      canvasTag.height = context.default_height;
      mainDiv.appendChild(canvasTag);
    }

    videoTag.addEventListener("play", () => {
      context.drawImage_Inteval = setInterval(() => {
        __drawImage(context);
        clearInterval(context.drawImage_Inteval);
        console.log("Drawing");
      }, 100);
    });
  }

  //Creating Button of Retake Photo
}

function __createRetakeButton(context){
    
  const mainDiv = context.shadowRoot.getElementById(context.mainHandDiv);
  const retakeId = context.retakeTagId;

  const retakeDiv = mainDiv.querySelector(`#${retakeId}`)

  if(retakeDiv){
    retakeDiv.style.display = "flex"
    return
  }
  console.log("Cretaing Retak");


  const retakeTag = document.createElement("div");
    // Setting Attributes
    Object.assign(retakeTag, {
      id: context.retakeTagId,
    });
    // Setting Style
    retakeTag.style.width =`${context.default_width}px`;
    retakeTag.style.height = "80px";
    retakeTag.style.display = "none";
    retakeTag.style.justifyContent = "center";
    retakeTag.style.alignItems = "center";

        const retakeButtonTag = document.createElement("button")
        retakeButtonTag.setAttribute("id", context.retakeButtonTag)
        retakeButtonTag.innerText = "Retake"
        retakeButtonTag.style.fontSize = "16px"
        retakeButtonTag.style.fontWeight = "bold"
        retakeButtonTag.style.padding = "20px 30px"
        retakeButtonTag.style.border = "none"
        retakeButtonTag.style.borderRadius = "10px"
        retakeButtonTag.style.cursor = "pointer"
        retakeButtonTag.style.color = "white"
        retakeButtonTag.style.backgroundImage = "linear-gradient(-180deg, #37AEE2 0%, #1E96C8 100%)"


        retakeButtonTag.addEventListener("click", ()=>{
            retakeTag.style.display = "none";
            context.handDetected = false;
            this.__drawImage(context)
        })


        retakeTag.appendChild(retakeButtonTag)

    mainDiv.appendChild(retakeTag)
  
}

function __startCamera(context) {
  let constraints = {
    audio: false,
    video: {
      facingMode: "user",
    },
  };

  navigator.mediaDevices
    .getUserMedia(constraints)
    .then((stream) => {
      const videoDiv = context.shadowRoot.querySelector(
        `#${context.videoTagId}`
      );
      videoDiv.srcObject = stream;
      window.localStream = stream;
    })
    .catch((err) => {
      console.log(err);
      alert("Error in accessing camera.");
    });
}

function __drawImage(context) {
  const videoId = context.videoTagId;
  const canvasId = context.canvasTagId;
  const loadingId = context.loadingTagId

  const videoDiv = context.shadowRoot.querySelector(`#${videoId}`);
  const canvasDiv = context.shadowRoot.querySelector(`#${canvasId}`);
  const loadingDiv = context.shadowRoot.querySelector(`#${loadingId}`);

  videoDiv.play();

    //Hiding the loader
    loadingDiv.style.display = "none"

  const ctx = canvasDiv.getContext("2d", { willReadFrequently: true });

  let curentTime = new Date();
  let animationFrameId = null

  let showingWavingHand = false;

  async function segmentFrame() {
    try {
      ctx.drawImage(videoDiv, 0, 0, canvasDiv.width, canvasDiv.height);

      if(!showingWavingHand){
        showingWavingHand = true;
            // Wave Hand
            createWavingGif(context)
            // Wave Hand
      }

      // Start:-- Calling Hand Detection after every 500 millisecond to decrese the Loading TIme
      let endTime = new Date();
      if (endTime - curentTime > 200 && !context.handDetected) {
        // console.log(endTime - curentTime);
        const [hand] = await context.loadedDetector.estimateHands(videoDiv, {
          flipHorizontal: true,
        });
        /*
            0: wrist,
            1: thumb_cmc,
            2: thumb_mcp,
            3: thumb_ip,
            4: thumb_tip,
            5: index_finger_mcp,
            6: index_finger_pip,
            7: index_finger_dip,
            8: index_finger_tip,
            9: middle_finger_mcp,
            10: middle_finger_pip,
            11: middle_finger_dip,
            12: middle_finger_tip,
            13: ring_finger_mcp,
            14: ring_finger_pip,
            15: ring_finger_dip,
            16: ring_finger_tip,
            17: pinky_finger_mcp,
            18: pinky_finger_pip,
            19: pinky_finger_dip,
            20: pinky_finger_tip,
        */
        if (hand) {
          const hand2DKeyPoints = hand.keypoints;
          const handedNess = hand.handedness 
        //   console.log(handedNess);
        //   console.log(hand);    
        //   console.log(hand2DKeyPoints);
          const wristKeyPoints = [
            { x: hand2DKeyPoints[0].x, y: hand2DKeyPoints[0].y },
          ];

          // Thumb [ Tip, IP, MCP, CMC]
          // Fingures [ Tip, DIP, PIP, MCP ]
          const tumbKeyPoints = [
            { x: hand2DKeyPoints[4].x, y: hand2DKeyPoints[4].y },
            { x: hand2DKeyPoints[3].x, y: hand2DKeyPoints[3].y },
            { x: hand2DKeyPoints[2].x, y: hand2DKeyPoints[2].y },
            { x: hand2DKeyPoints[1].x, y: hand2DKeyPoints[1].y },
          ];
          const indexKeyPoints = [
            { x: hand2DKeyPoints[8].x, y: hand2DKeyPoints[8].y },
            { x: hand2DKeyPoints[7].x, y: hand2DKeyPoints[7].y },
            { x: hand2DKeyPoints[6].x, y: hand2DKeyPoints[6].y },
            { x: hand2DKeyPoints[5].x, y: hand2DKeyPoints[5].y },
          ];
          const middleKeyPoints = [
            { x: hand2DKeyPoints[12].x, y: hand2DKeyPoints[12].y },
            { x: hand2DKeyPoints[11].x, y: hand2DKeyPoints[11].y },
            { x: hand2DKeyPoints[10].x, y: hand2DKeyPoints[10].y },
            { x: hand2DKeyPoints[9].x, y: hand2DKeyPoints[9].y },
          ];
          const ringKeyPoints = [
            { x: hand2DKeyPoints[16].x, y: hand2DKeyPoints[16].y },
            { x: hand2DKeyPoints[15].x, y: hand2DKeyPoints[15].y },
            { x: hand2DKeyPoints[14].x, y: hand2DKeyPoints[14].y },
            { x: hand2DKeyPoints[13].x, y: hand2DKeyPoints[13].y },
          ];
          const pinyKeyPoints = [
            { x: hand2DKeyPoints[20].x, y: hand2DKeyPoints[20].y },
            { x: hand2DKeyPoints[19].x, y: hand2DKeyPoints[19].y },
            { x: hand2DKeyPoints[18].x, y: hand2DKeyPoints[18].y },
            { x: hand2DKeyPoints[17].x, y: hand2DKeyPoints[17].y },
          ];
        //   console.log("wristKeyPoints", "tumbKeyPoints", "indexKeyPoints", "middleKeyPoints", "ringKeyPoints", "pinyKeyPoints");
        //   console.log(wristKeyPoints, tumbKeyPoints, indexKeyPoints, middleKeyPoints, ringKeyPoints, pinyKeyPoints);
        // console.log(tumbKeyPoints[0].x, indexKeyPoints[0].x, middleKeyPoints[0].x, ringKeyPoints[0].x, pinyKeyPoints[0].x);
        
        /*
            IMPORTANT:-- Here y of tip is smaller than low
            Considering Right Hand
                Cond1:- Tip of Thumb to Pink Fingure will always be increasing (Right) / descreasing(Left)
                Cond2:- Y coordinate of Tip < Bottom of Tip  (Both Left and Right)
        */

         //Cond1 --> Thumb to Pinky
         const tipXCord = [tumbKeyPoints[0].x, indexKeyPoints[0].x, middleKeyPoints[0].x, ringKeyPoints[0].x, pinyKeyPoints[0].x]
         let isPalm = true;

         if(handedNess.toUpperCase() === "RIGHT" || handedNess.toUpperCase() === "LEFT" 
        ){
                // Should be increasing -- Right
                // Should be decreasing -- Left
                for(let i=0; i<=tipXCord.length; i++){
                    if((handedNess.toUpperCase() === "RIGHT" && tipXCord[i] > tipXCord[i+1]) || (handedNess.toUpperCase() === "LEFT" && tipXCord[i] < tipXCord[i+1])){
                        isPalm = false
                        break;
                    }

                }
         }
            // Tip < Low
         const thumbDiff = tumbKeyPoints[0].y < tumbKeyPoints[3].y
         const indexDiff = indexKeyPoints[0].y < indexKeyPoints[3].y
         const middleDiff = middleKeyPoints[0].y < middleKeyPoints[3].y
         const ringDiff = ringKeyPoints[0].y < ringKeyPoints[3].y
         const pinkyDiff = pinyKeyPoints[0].y < pinyKeyPoints[3].y

         //  console.log(thumbDiff, indexDiff, middleDiff, ringDiff, pinkyDiff);
         if(isPalm && thumbDiff && indexDiff && middleDiff && ringDiff && pinkyDiff){
            //  console.log(handedNess, isPalm, "inside", animationFrameId);

                console.log("hand detected");
                context.shadowRoot.querySelector(`#${context.wavingHandTagId}`).style.display = "none"
                createCoutnerGif(context)
                context.handDetected = true
                setTimeout(()=>{
                    cancelAnimationFrame(animationFrameId)
                    context.shadowRoot.querySelector(`#${context.counterTagId}`).style.display = "none"
                    __createRetakeButton(context)
                    return

                }, 3000)
        }
        else{
            isPalm = false
        }

        //  if(thu)


          
        }
        curentTime = new Date();

      }
      // End:--    Calling Hand Detection after every 500 millisecond to decrese the Loading TIme
    } catch (err) {
      console.log(err);
    }
    animationFrameId = requestAnimationFrame(segmentFrame);
    // console.log(animationFrameId);
  }

  const segmentInteval = setInterval(() => {
    if (context.loadedDetector) {
      clearInterval(segmentInteval);
      segmentFrame();
    }
  }, 100);
}

function createWavingGif(context){
    const mainDiv = context.shadowRoot.getElementById(context.mainHandDiv); 
    
    const checkWavingHandDiv = context.shadowRoot.getElementById(`${context.wavingHandTagId}`);
    
    if(checkWavingHandDiv){
        checkWavingHandDiv.style.display = "block";
        return
    }

    console.log("Cretaing Wave");

    //Creating Waving Hand GIF
    const wavingHandDiv = document.createElement("div");
    wavingHandDiv.setAttribute("id", context.wavingHandTagId)
    wavingHandDiv.style.position = "absolute";
    wavingHandDiv.style.top = `${context.default_height * 0.5}px`;
    wavingHandDiv.style.opacity = 0.7;
    wavingHandDiv.style.width = `${Number(context.default_width)}px`
    wavingHandDiv.style.height = `${context.default_height * 0.5}px`
    wavingHandDiv.style.display = `flex`
    wavingHandDiv.style.justifyContent = `center`
    wavingHandDiv.style.alignItems = `center`
        // Crete Loading Text
      const wavingGif =document.createElement('img')
      wavingGif.src = "./assets/waveHand.gif"
      wavingGif.width = "300"
      wavingHandDiv.appendChild(wavingGif)
    mainDiv.appendChild(wavingHandDiv);
}

function createCoutnerGif(context){
    const mainDiv = context.shadowRoot.getElementById(context.mainHandDiv);     
    
    const checkCounterDiv = context.shadowRoot.getElementById(`${context.counterTagId}`);

    if(checkCounterDiv){
        checkCounterDiv.style.display = "flex";
        return
    }

    console.log("Cretaing Counter", checkCounterDiv);

    //Creating Waving Hand GIF
    const counterDiv = document.createElement("div");
    counterDiv.setAttribute("id", context.counterTagId)
    counterDiv.style.position = "absolute";
    counterDiv.style.top = `0`;
    counterDiv.style.opacity = 0.7;
    counterDiv.style.width = `${Number(context.default_width)}px`
    counterDiv.style.height = `${context.default_height}px`
    counterDiv.style.display = `flex`
    counterDiv.style.justifyContent = `center`
    counterDiv.style.alignItems = `center`
        // Crete Loading Text
      const counterGif =document.createElement('img')
      counterGif.src = "./assets/counter.gif"
      counterGif.width = "300"
      counterDiv.appendChild(counterGif)
    mainDiv.appendChild(counterDiv);
}

async function __loadHandPose(context) {
  try {
    const model = handPoseDetection.SupportedModels.MediaPipeHands;
    context.loadedDetector = await handPoseDetection.createDetector(
      model,
      context.detectorConfig
    );
  } catch (err) {
    console.log(err);
  }
}

class HandPoseDetection extends HTMLElement {
  constructor() {
    super();
    this.context = this;
    //Widht and Height
    this.default_width = "355";
    this.default_height = "500";
    // Id's for video and canvas
    this.mainHandDiv = "mainDivHandPose"
    this.videoTagId = "videoDiv";
    this.canvasTagId = "canvasDiv";
    this.loadingTagId = "loadingDiv";
    this.wavingHandTagId = "wavingHandDiv";
    this.counterTagId = "counterDiv";
    this.retakeTagId = "retakeDiv";
    this.retakeButtonTagId = "retakeButton";

    //Interval
    this.drawImage_Inteval = null;
    //Detector Config
    this.loadedDetector = null;
    this.detectorConfig = {
      runtime: "mediapipe", // or 'tfjs'
      modelType: "full",
      maxHands: 1,
      solutionPath: `https://cdn.jsdelivr.net/npm/@mediapipe/hands`,
      // solutionPath: `https://cdn.jsdelivr.net/npm/@mediapipe/hands@0.4.1635986972`
    };
    this.handDetected = false;

    //Creating a Shadow Root
    const shadowRoot = this.attachShadow({ mode: "open" });

    //1. Main Div
    const mainDiv = document.createElement("main");
    // Setting Attributes
    Object.assign(mainDiv, {
      id: this.mainHandDiv,
    });
    // Setting Style
    mainDiv.style.width = "fit-content";
    mainDiv.style.height = "fit-content";
    mainDiv.style.position = "absolute";
    mainDiv.style.left = "50%";
    mainDiv.style.top = "10%";
    mainDiv.style.marginLeft = `-${this.default_width / 2}px`;

    // mainDiv.innerText = "Hii I am a main Div";

    shadowRoot.appendChild(mainDiv);
  }

  connectedCallback() {
    __loadHandPose(this.context);
    __createOrUpdateVideo(this.context);
    __createRetakeButton(this.context)
    __startCamera(this.context);
  }
}

customElements.define("hand-pose-detection", HandPoseDetection);
